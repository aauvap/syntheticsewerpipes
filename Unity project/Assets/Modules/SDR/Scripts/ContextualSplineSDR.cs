﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ContextualSpline takes the values from global parameters and instantiates objects based on the controlpoint informations.
/// Hieracy: ScenarioSDR --> GlobalParameters --> (ContextualSpline) --> Objects
/// </summary>
public class ContextualSplineSDR : StructuredDomainRandomization{

    ///Global variables to be accessed throughout the script. These variables are modified from the GlobalParameters using the Setup function. 
    public int numberOfControlPoints;
    public ControlPoint[] controlPoints;
    public bool contextIsDefect;
    public float defectProbability;

    ///Possible defect objects that can be accessed in this script. The objects are shown in the editor.
    public GameObject[] defectRubberRings;

    /// <summary>
    /// This function is accessed by the GlobalParameters class and passes the following values;
    /// The number of controlpoints of the selected scenario.
    /// The array of controlpoint struct, which includes information shown in "StructureDomainRandomization" class. 
    /// Wether the Contextual spline is a defect.
    /// And the probability of having a defect, which is also from the selected scenario.
    /// </summary>
    public void Setup(int numberOfControlPoints, ControlPoint[] controlPoints, bool contextIsDefect, float defectProbability){
        ///The following lines saves the values from the parameters into the global parameters.
        this.numberOfControlPoints = numberOfControlPoints; 
        this.controlPoints = controlPoints;
        this.contextIsDefect = contextIsDefect;
        this.defectProbability = defectProbability;

        Random.seed = (int)System.DateTime.Now.Ticks; /// Since random is never random, a new seed for the random generator is calculated using the time of the computer to enhance randomization.
        CreateObjects();
    }

    ///////////////////////
    // Objects
    //////////////////////

    /// <summary>
    /// Creates objects based on the context of the spline
    /// </summary>
    private void CreateObjects(){

        ///If context is not defect
        if(!contextIsDefect){
            CreateSewerSystem();
        }

        ///If context is defect
        if(contextIsDefect){
            CreateRubberRings();
        }
    }

    /// <summary>
    /// If context is not defect, then it create the sewer system, which consists of the pipes and the endblock to cover the camera from seeing outside of the sewer system.
    /// </summary>
    private void CreateSewerSystem(){
        for (int i = 0; i < numberOfControlPoints; i++) {
             ///If a controlpoint doesn't contain a pipe, then it will not go through the following code.
             ///If so, it will instantiate the given pipe prefab and then set its parent to an object container of this contextual spline.
            if (controlPoints[i].pipe != null) {
                GameObject newGameObject = Instantiate(controlPoints[i].pipe);
                newGameObject.transform.parent = this.transform.Find("Objects");
            }
            ///If a controlpoint doesn't contain a endblock, then it will not go through the following code.
            ///If so, it will instantiate the given endblock prefab and then set its parent to an object container of this contextual spline.
            if (controlPoints[i].endBlock != null) {
                GameObject newGameObject = Instantiate(controlPoints[i].endBlock);
                newGameObject.transform.parent = this.transform.Find("Objects");
            }
        }
    }


    /// <summary>
    /// If context is a defect, then it will create rubberrings based
    /// </summary>
    private void CreateRubberRings(){
       
        for (int i = 0; i < numberOfControlPoints; i++) { ///Loops through the number of how many control points there are. 

            ///If the controlpoint isn't displaced, then continue, since SDR assumes that machine learns through context, 
            ///and rubber rings are most likely not shown if displacement of pipes doesn't occur. 
            if (controlPoints[i].displacedPipe == false){ continue; }
            
            ///To instantiate a random rubber ring, a random value is found, where the maximum is the number of rubber ring varieties + 1.
            ///The +1 is due to when using int, the maximum is exclusive, while float is inclusive.
            int randomValue = Random.Range(0, 5); 

            GameObject newRubberRing = Instantiate(defectRubberRings[randomValue]); ///A newly instantiated rubber ring is temporary saved into a gameobject, to access it in this script.
            newRubberRing.transform.parent = this.transform.Find("Objects"); ///Places the new rubber ring as a child to the object container of this context spline.

            Transform pipehead = controlPoints[i].pipe.transform.Find("Head").parent; ///Accesses the controlpoint of where the new rubber ring should occur due to displacement.
            newRubberRing.transform.position = pipehead.position+new Vector3(0,0,-0.05f); ///Transforms the position of the new rubber ring to the displacment location.
            newRubberRing.transform.rotation = pipehead.rotation; ///Transforms the rotation of the new rubber ring to the rotation of the control point.
            newRubberRing.transform.Rotate(new Vector3(90, 0, 0), Space.Self); ///90 degrees to let the rubber ring not "lay down", but instead be vertical. Hope it makes sense :) 
            newRubberRing.transform.Rotate(new Vector3(0, Random.Range(0f,360f), 0), Space.Self); ///90 degrees to let the rubber ring not "lay down", but instead be vertical. Hope it makes sense :) 
            newRubberRing.transform.localScale = new Vector3(Random.Range(90f,110f),Random.Range(90f,110f),Random.Range(90f,110f));
        }
    }


}
