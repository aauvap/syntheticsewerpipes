﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// GlobalParameters takes the values from the chosen scenario, and creates splines based on this information.
/// Hieracy: ScenarioSDR --> (GlobalParameters) --> ContextualSpline --> Objects
/// </summary>
public class GlobalParametersSDR : StructuredDomainRandomization{

    ///The following gameobjects are given in the editor, which allows this class to instantiate them.

    public GameObject contextualSpline; ///Contains the contextual spline prefab.
    public GameObject blockCamera; ///Contains the model that blocks the camera from seeing outside the system.
    public GameObject[] pipeModels; ///Contains all the possible objects that can be instantiated.
    
    /// <summary>
    /// Sets the values for the GlobalParameter class, this is only called once in ScenarioSDR class. 
    /// </summary>
    public void SetGlobalParameters(int numberOfControlPoints, float turnProbability, float defectProbability){
        ///Collects information for controlpoint structs and returns the array.
        ControlPoint[] controlPoints = CollectControlPointsFromTemporaryInstantiatedPipes(numberOfControlPoints, turnProbability, defectProbability);

        ///Creates a contextual spline based on the controlpoints. Moreover, parameters are given:
        ///numberofControlPoints and defectProbability is from the selected Scenario in the class "ScenarioSDR".
        ///controlPoints is the array of the structure "ControlPoint" which contains the information shown in class "StructuredDomainRandomization".
        ///the thrid parameter, is the amount of offset.
        ///The fourth parameter, is to indicate if the spline should have object defects in it.
        CreateContextualSpline(numberOfControlPoints, controlPoints, 0f, false, defectProbability); ///Not defect Contextual Spline, only contains pipes.

        ///Creates a defect contextual spline, and has the same parameters as below, but with other values.
        if (defectProbability > 0) {
            CreateContextualSpline(numberOfControlPoints, controlPoints, 0.2f, true, defectProbability); ///Is defect Contextual Spline, does not contain pipes, but defect objects.
        }
    }

    /// <summary>
    /// This function temporary instantiates pipes to collect information for the controlpoint structs, which is being used later during the creation of the splines.
    /// It returns an array of the controlpoint struct, which can be seen in the class "StructuredDomainRandomization".
    /// </summary>
    private ControlPoint[] CollectControlPointsFromTemporaryInstantiatedPipes(int numberOfControlPoints, float turnProbability, float defectProbability){
        int numberOfPipes = numberOfControlPoints / 4; ///To get the number of pipes, the number of controlpoints is devided by four, as there are four controlpoints for each pipe.
        ControlPoint[] controlPoints = new ControlPoint[numberOfControlPoints]; ///This array contains empty controlpoints with the size of the number of controlpoints.
        
        ///Here pipes are randomly instantiated, then the controlpoints of the instanted pipe are obtained and saved into the empty array of control points. 
        ///When all controlpoints of the pipe are obtained, the pipe is destroyed. This is desired, as only the controlpoints are of interest at this point.
        ///The controlpoints can then be transformed without also having the need to process the pipes.
        for (int i = 0; i < numberOfPipes; i++) {

            ///Function which instantiates and returns a new pipe gameobject. It passes the turn probability from the selected scenario to be used as a probability to instantiate turn pipes. 
            ///The 'i' is the ID for the pipe. IDs are important, as the function below, will try and access prevously instantiated pipes, which is not desired, if it's the first pipe in the system.
            GameObject newPipe = SpawnPipeObject(turnProbability, i);

            ///Displacement of pipes are found in the code block below.
            float randomValue = Random.Range(0f, 100f); ///Here a random value is found between 0 and 100.
            if (randomValue <= defectProbability) { ///If the random value is less or equal to the probability of defects, then a displacement defect will occur.
                newPipe.transform.Translate(new Vector3(0, 0, -0.15f), Space.Self); ///The pipe will be translated in local coordinates to displace the pipe. This can further be randomized, by randomizing the z value.
                controlPoints[(4 * i) + 0].displacedPipe = true; ///To indicate that a displacement occurs, the controlpoint of where the rubber-ring is placed is set to true. This allows to later instantiate rubber-ring defects in this control-point position.
            }

            controlPoints[(4 * i) + 0].pipe = newPipe; ///The starting controlpoint of a pipe, will save the newly instantiated pipe, to late be instantiated again, after processing the splines.

            ///Here all the pivot points in the newly instantiated pipe, is saved into positions of the array of controlpoints.
            controlPoints[(4 * i) + 0].position = newPipe.transform.Find("BendStart").position; 
            controlPoints[(4 * i) + 1].position = newPipe.transform.Find("BendMid").position;
            controlPoints[(4 * i) + 2].position = newPipe.transform.Find("BendHead").position;
            controlPoints[(4 * i) + 3].position = newPipe.transform.Find("Head").position;

            Destroy(newPipe); ///This is a build-in function, which destroys the temporary gameobject.

            ///Since it's desired to not allow the camera to look outside the pipe system, the last control-point of the last pipe will contain an object which blocks the view of the outside.
            if(i == numberOfPipes-1){
                blockCamera.transform.position = controlPoints[(4 * i) + 3].position; ///Sets the position of the block to the last control point.
                blockCamera.transform.LookAt(controlPoints[(4 * i) + 2].position); ///Will rotate the block so its forward vector points in the direction of the control point.
                controlPoints[(4 * i) + 3].endBlock = blockCamera; ///The endblock will have the blocking model.
            }
        }
        return controlPoints; ///Returns the array of controlpoints.
    }

    
    /// <summary>
    /// This function creates a contextual spline based on the given parameters. This function instantiates, set-up and transforms the spline based on position offsets. 
    /// </summary>
    private void CreateContextualSpline(int numberOfControlPoints, ControlPoint[] controlPoints, float amountOffset, bool contextIsDefect, float defectProbability){
        GameObject newContextualSplineGameObject = Instantiate(contextualSpline); ///Instantiates a new spline and temporarly saves the gameobject to allows to access and modify the gameobject.
        ContextualSplineSDR newContextualSplineScript = newContextualSplineGameObject.GetComponent<ContextualSplineSDR>(); ///Temporarly saves the script of the accessed spline gameobject.
        newContextualSplineGameObject.transform.parent = this.transform.Find("ContextualSplines"); ///Here the spline is set to a child of a container that contains only splines, to easier get an overview in the editor window.


        newContextualSplineScript.Setup(numberOfControlPoints, controlPoints, contextIsDefect, defectProbability); ///This passes all the values into the script of the newly instantiated spline. These values are the ones from ScenarioSDR.

        LineRenderer lineRenderer = newContextualSplineGameObject.GetComponent<LineRenderer>(); ///Here the LineRenderer component of the newly instantiated spline is temporarly saved. LineRender allows to draw a visual spline (visually debugging of where the spline is located) and can be accessed by the camera as a guideline.
        lineRenderer.positionCount = numberOfControlPoints; ///Depending on how many number of control points the scenario is given, the linerenderer component should contain the same number, else a spline would not be able to be created.

        //Random.seed = (int)System.DateTime.Now.Ticks; ///(NOT USED ANYMORE) Since random is never random, a new seed for the random generator is calculated using the time of the computer to enhance randomization.

        ///Goes through all the controlpoints and gives it a offset, which translates all the controlpoints (the spline) in a y-direction. If offset is above 0, it's due to defects.
        for (int i = 0; i < numberOfControlPoints; i++) {
            lineRenderer.SetPosition(i, controlPoints[i].position+new Vector3(0,amountOffset,0));
        }
    }

    /// <summary>
    /// This function randomly instantiates a pipe object based on turnprobability; turn pipe or straight pipe. 
    /// Moreover, transforms the new pipe based on previously instantiated pipes, to correctly connect the new pipe with the previous pipe in world coordinates.
    /// In the end, the newly instantiated pipe gameobject is returned.
    /// </summary>
    private GameObject SpawnPipeObject(float turnProbability, int pipeIdx){
        GameObject newPipe; ///Temporarly creates an empty gameobject, which is used to save the newly instantiated pipe.

        ///The block of code below instantiates a new gameobject based on random probabilities.
        float randomValue = Random.Range(0f, 100f); ///A random value is found from 0 to 100.
        if (randomValue > turnProbability) { ///Here the turn probability will be used as a threshold. If the random value above, is higher than the threshold for example 10%
            newPipe = Instantiate(pipeModels[0]); ///Instantiates the new straigt pipe and temporary saves it in the empty gameobject newpipe.
        } else { ///If the random value is equal or below the threshold, then it will be a turned pipe.
            int randomSelectTurnPipe = Random.Range(1, 7); ///A new random value is obtained based on the number of variety of turn pipes (6). Notice, since return type is int, max (7) is exclusive.
            newPipe = Instantiate(pipeModels[randomSelectTurnPipe]); ///Instantiates the new turn pipe and temporary saves it in the empty gameobject newpipe.
        }


        ///To easier get an overview of where all the pipes are located in the editor, each pipe will get the TempPipeContainer as the parent. 
        ///Moreover, the pipes in this container can be accessed and processed through IDs later, where the first child will have ID = 0, the next = 1...
        newPipe.transform.SetParent(this.transform.Find("TempPipeContainer"));

        ///Instantiates three "empty" vector3s to temporary save information later about the previous instantiated pipe.
        Vector3 previousPipeRotation = new Vector3(0, 0, 0); 
        Vector3 previousRotateOutput = new Vector3(0, 0, 0);
        Vector3 previousPipePosition = new Vector3(0, 0, 0);

        string previousPipeName = null; ///contains the name of the previous pipe.
        if (pipeIdx != 0) { ///Since information about the previous instantiated pipe, is not necesary and will give an error if no pipes have been instantiated, the first pipe will not run the following code.
            Transform previousPipe = this.transform.Find("TempPipeContainer").GetChild(pipeIdx - 1); ///Temporary saves the previous pipe into a transform variable, by accessing the current child id - 1 in the pipe container.
            previousPipeName = this.transform.Find("TempPipeContainer").GetChild(pipeIdx - 1).name; ///Accesses and saves the name of the previous pipe.
            previousPipeRotation = previousPipe.rotation.eulerAngles; ///Accesses and saves the rotation of the pipe in euler angles. Used to know how much to rotate the new instantiated object, to correctly connect them.
            previousPipePosition = previousPipe.transform.Find("Head").position; ///Accesses and saves the position of head (child:controlpoint) in the previous pipe. Used to know how to translate the new instantiated object, to correctly connect them.
        }

        ///Firstly, the position of the newly instantiated pipe is set to the position of the previous pipe in world space.
        newPipe.transform.position = previousPipePosition;

        ///Secondly, the newly instantiated pipe is rotated in world space based on the name from the previous instantiated pipe and the amount of rotation the end of the previous pipe will have.
        switch (previousPipeName) {
            case "PipeStraight(Clone)":
                newPipe.transform.Rotate(previousPipeRotation, Space.World);
                break;
            case "PipeBend15L(Clone)":
                newPipe.transform.Rotate(previousPipeRotation + new Vector3(0, 15, 0), Space.World);
                break;
            case "PipeBend15R(Clone)":
                newPipe.transform.Rotate(previousPipeRotation + new Vector3(0, -15, -180), Space.World);
                break;
            case "Bend30L400mm(Clone)":
                newPipe.transform.Rotate(previousPipeRotation + new Vector3(0, 30, -180), Space.World);
                break;
            case "Bend30R400mm(Clone)":
                newPipe.transform.Rotate(previousPipeRotation + new Vector3(0, -30, 0), Space.World);
                break;
            case "PipeBend45L(Clone)":
                newPipe.transform.Rotate(previousPipeRotation + new Vector3(0, 45, 0), Space.World);
                break;
            case "PipeBend45R(Clone)":
                newPipe.transform.Rotate(previousPipeRotation + new Vector3(0, -45, -180), Space.World);
                break;
            case "Straight400D1000L(Clone)":
                newPipe.transform.Rotate(previousPipeRotation, Space.World);
                break;
            case "Straight400D2000L(Clone)":
                newPipe.transform.Rotate(previousPipeRotation, Space.World);
                break;
             case "Straight400D3000L(Clone)":
                newPipe.transform.Rotate(previousPipeRotation, Space.World);
                break;
             case "Straight400D6000L(Clone)":
                newPipe.transform.Rotate(previousPipeRotation, Space.World);
                break;
        }
        return newPipe; ///Returns the newly instantiated object.
    }

}
