﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructuredDomainRandomization : MonoBehaviour{

    /// <summary>
    /// This struct allows the derived classes to pass multiple variables. 
    /// Pipe includes which object the control point contains, this can either be none or one of the pipes that have been created.
    /// Endblock is only for the last control point, to stop the camera from looking outside the pipe system, else parts of the image will become black.
    /// DisplacedPipe is true if a displacement occurs, this is used to check if a rubber ring is allowed to be spawned at that control point.
    /// Position is the position of the control point in world coordinates.
    /// </summary>
    public struct ControlPoint{
        public GameObject pipe;
        public GameObject endBlock;
        public bool displacedPipe;
        public Vector3 position;
    };
}
