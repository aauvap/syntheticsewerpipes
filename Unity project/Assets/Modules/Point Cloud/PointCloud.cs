﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCloud : MonoBehaviour{
    //public SimulationSettings simulationSettings; ///Contains information about the screen size.
    public ParticleSystem particleSystem; ///The particle system which is used for generating the point cloud
    ParticleSystem.Particle[] particles; ///An array that will contain the particles in the point cloud
    private List<Vector4> customData = new List<Vector4>(); ///Not Used Anymore

    public bool pointCloudIsReady; ///Used to indicate if the point cloud is successfully generated.

    void Start(){
        pointCloudIsReady = false; 
    }

    private void Update(){
        CheckIfCloudIsReady();
    }

    /// <summary>
    /// Checks if the pointcloud is ready. This is used for beginning taking screenshots in PicoFlexxSensor.
    /// </summary>
    private void CheckIfCloudIsReady(){
        if (particleSystem.particleCount > 0) {
            pointCloudIsReady = true;
        }
    }

    /// <summary>
    /// Takes the positions of where each ray hits a collider as parameter and then translates each particle to that position.
    /// Called from PicoFlexxSensor.
    /// </summary>
    /// <param name="rayHitPositions"></param>
    public void SetAllParticlesPositions(Vector3[] rayHitPositions){
        if(particleSystem.particleCount == 0){
            particles = new ParticleSystem.Particle[rayHitPositions.Length];
            particleSystem.emission.SetBursts(
            new ParticleSystem.Burst[]{
                new ParticleSystem.Burst(0.0f,(SimulationSettings.imageWidth/SimulationSettings.pixelsEachRayCovers)*(SimulationSettings.imageHeight/SimulationSettings.pixelsEachRayCovers ))
            });
        }

        int particlesAlive = particleSystem.GetParticles(particles);
        for (int i = 0; i < particlesAlive; i++){
            particles[i].position = rayHitPositions[i];
            particles[i].remainingLifetime = 100;
        }

        particleSystem.SetParticles(particles,particles.Length);
    }

    /// <summary>
    /// Takes the origin of the camera, direction of where each ray and distance from camera to hit as parameter and then translates each particle to that position.
    /// Called from PicoFlexxSensor.
    /// </summary>
    /// <param name="rayHitPositions"></param>
    public void SetAllParticlesPositions(Vector3 cameraOrigin,PicoFlexxSensor.Point[] points){
        if(particleSystem.particleCount == 0){
            particles = new ParticleSystem.Particle[points.Length];
            particleSystem.emission.SetBursts(
            new ParticleSystem.Burst[]{
                new ParticleSystem.Burst(0.0f,(SimulationSettings.imageWidth/SimulationSettings.pixelsEachRayCovers)*(SimulationSettings.imageHeight/SimulationSettings.pixelsEachRayCovers ))
            });
        }

        int particlesAlive = particleSystem.GetParticles(particles);
        for (int i = 0; i < particlesAlive; i++){
            if(points[i].direction == new Vector3(0,0,0)){
                particles[i].position = new Vector3(0,10,0);
                continue;
            }

            particles[i].position = cameraOrigin+points[i].direction*(points[i].distance);


            if (particles[i].position == new Vector3(0, 0, 0)) {
                particles[i].position = new Vector3(0, 10, 0);
            }

            particles[i].remainingLifetime = 100;
        }
        particleSystem.SetParticles(particles,particles.Length);
    }
}
