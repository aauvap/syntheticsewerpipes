﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataGenerator : MonoBehaviour{
    public Camera pointcloud_camera; ///Point Cloud Camera.

    ///Desired number of fine and defect images. 
    ///This is used to stop when the desired number of a label is obtained, else it might be come a unbalanced dataset. 
    public int numberOfImagesFine, numberOfImagesDefect;


    public bool GenerateCSV(PicoFlexxSensor.Point[] point_array, bool foundDefect){
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US"); ///To use dots instead of commas, else the comma to indicate float points will be though as seperation symbols instead.

        List<PicoFlexxSensor.Point> pointList = new List<PicoFlexxSensor.Point>();

        string folderPath = ObtainPathDependingOnFoundDefect(foundDefect); ///A function which find if it's a defect or not, and then returns a path to put the image into a folder of it's classification.
        if (folderPath == "Done") { return true; }; ///Returns true, if the desired number of images has been created already.
        int countNumberOfFilesInFolder = System.IO.Directory.GetFiles(folderPath).Length + 1; ///Counts the number of files in the folder and saves it as an id to ensure not overwriting, if generates multiple times to the same folders.

        for (int i = 0; i < point_array.Length; i++) {
            float distance = point_array[i].distance;
            if (distance != 0) {
                PicoFlexxSensor.Point point = point_array[i];
                pointList.Add(point);
            }
        }

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(folderPath+"/csv"+countNumberOfFilesInFolder+".csv", true)) {
            for (int i = 0; i < pointList.Count; i++) {
                PicoFlexxSensor.Point point = pointList[i];

                Vector3 impactPoint = point.impactPoint;
                float distance = point.distance;
                

                file.WriteLine(impactPoint.x + ", " + impactPoint.y + ", " + impactPoint.z + ", " + point.defect);
            }
        }

        return true;
    }


    /// <summary>
    /// Generates Image from the pointcloud camera by rendering the camera and then writes the image to the desired path.
    /// This function is called in PicoFlexxSensor.
    /// </summary>
    /// <param name="foundDefect"></param>
    /// <returns>Returns true if image is written successfully or enough images of a label has been created. Has to return true, else the camera will not move forward. </returns>
    public bool GenerateImage(bool foundDefect){
        ///Rendering Part
        RenderTexture renderTexture = new RenderTexture(SimulationSettings.imageWidth, SimulationSettings.imageHeight, 24); ///Width, Height and Depth.
        pointcloud_camera.targetTexture = renderTexture; ///This allows the point cloud camera to render into a texture, instead of directly to the screen.
        Texture2D screenShot = new Texture2D(SimulationSettings.imageWidth, SimulationSettings.imageHeight, TextureFormat.RGB24, false); ///Texture information.
        pointcloud_camera.Render(); ///Allows to manual control the rendering process.
        RenderTexture.active = renderTexture; ///A texture that can be rendered to using the camera.
        screenShot.ReadPixels(new Rect(0, 0, SimulationSettings.imageWidth, SimulationSettings.imageHeight), 0, 0); ///It creates a new rectangle of the width and height of scene. Moreover, it reads the pixels to that rectangle.
        ///Continue Rendering
        pointcloud_camera.targetTexture = null; ///Stops the manual rendering process, and renders to the scene.
        RenderTexture.active = null; ///Renders to main window again.
        Destroy(renderTexture); ///Destroys the rendertexture, to stop allocating memory for the variable.
        byte[] bytes = screenShot.EncodeToJPG();  ///Encodes a JPG texture to be rendered to.

        ///Obtain Path
        string path = ObtainPathDependingOnFoundDefect(foundDefect); ///A function which find if it's a defect or not, and then returns a path to put the image into a folder of it's classification.
        if (path == "Done") { return true; }; ///Returns true, if the desired number of images has been created already.

        ///Write Image to path
        int imageID = System.IO.Directory.GetFiles(path).Length+1; ///Counts the number of files in the folder and saves it as an id to ensure not overwriting, if generates multiple times to the same folders.
        System.IO.File.WriteAllBytes(path + "/screenshot"+imageID+".jpg", bytes); ///Here it writes the jpg screenshot and gives it a name.
        return true; ///Returns true, to indicate that the rendering of an image was successfull.
    }



    /// <summary>
    /// Check if a defect is found or not, which will return a path based on the value. 
    /// Moreover, if the desired number of images for a label is obtained, it will return "Done", which is used for skipping the rendering part.
    /// </summary>
    /// <param name="foundDefect"></param>
    /// <returns></returns>
    private string ObtainPathDependingOnFoundDefect(bool foundDefect){
        string path = "";
        if (foundDefect == true) {
            path = SimulationSettings.directory_path + "/Defect";
            if (numberOfImagesDefect <= 0) {
                return "Done";
            }
            numberOfImagesDefect--;
        } else {
            path = SimulationSettings.directory_path + "/Fine";
            if (numberOfImagesFine <= 0) {
                return "Done";
            }
            numberOfImagesFine--;
        }
        return path;
    }

    /// <summary>
    /// Updates of still desired number of images for fine and defects. These values are accessed by the UI.
    /// </summary>
    /// <param name="temp_numberOfImagesFine"></param>
    /// <param name="temp_numberOfImagesDefects"></param>
    public void SetNumberOfDesiredImages(int temp_numberOfImagesFine, int temp_numberOfImagesDefects){
        numberOfImagesFine = temp_numberOfImagesFine;
        numberOfImagesDefect = temp_numberOfImagesDefects;
    }
}
