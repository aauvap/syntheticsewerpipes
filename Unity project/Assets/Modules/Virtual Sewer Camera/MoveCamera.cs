﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour{
    private Vector3 nextPostition;
    private int i;
    Vector3[] positions;
    public Transform splines;
    public LineRenderer pipeSpline;
    bool gotPositions;
    // Start is called before the first frame update
    void Start(){
        gotPositions = false;

        i = 1;
    }

    void Update(){
        ///Through each update it will check if it has got positions from the spline. It can happen, that the contextual splines are not ready when it goes through this update.
        if(gotPositions == false){
            pipeSpline = splines.GetChild(0).GetComponent<LineRenderer>(); ///Saves the LineRenderer component of the first spline.
            if (pipeSpline.positionCount>0){ ///If the LineRenderer isn't empty, then save all the positions from the LineRenderer into an array of positions.
                Vector3[] newPos = new Vector3[pipeSpline.positionCount];
                pipeSpline.GetPositions(newPos);
                positions = newPos;
                gotPositions = true; ///Ensures that it only do the code once.
            }
        }
    }

    /// <summary>
    /// This is called in "PicoFlexxSensor" class when a image is successfully generated. This allows the camera to move forward in the contextual spline.
    /// </summary>
    public void UpdateCameraMovement(){
        if(gotPositions!=true){return; } ///If it hasn't got positions from the spline yet, then return.

        if (positions.Length != 0){
           
            ///Checks if the camera has reached it's destination, if so, it will set a new destination for the next controlpoint in the spline.
            if(transform.position == nextPostition){
                i++;
                if(positions.Length>i) 
                {
                    ///Debug.Log("Length: "+ positions.Length + " i:" + i);
                    nextPostition = positions[i];
                }
                else
                {
                    print("No more pipe, add some more");
                }
            }   
            

            float step =  (SimulationSettings.translationSpeed + Random.Range(-0.1f,0.1f)) * Time.deltaTime; /// Calculate the amount of distance to move the camera
            Vector3 moveTowards = Vector3.MoveTowards(transform.position ,nextPostition,step); ///Calculates the position from the camera position towards it's destination with the step distance value.
            transform.position = moveTowards; ///Moves the camera to the calculated position above.
            
            Vector3 rotateTowards = Vector3.RotateTowards(transform.forward,(nextPostition),step,0.0f); ///Calculates the rotation from the camera to the destination using the step value again. Better description of the function in the Unity manual.
            transform.rotation = Quaternion.LookRotation(rotateTowards); ///Sets the rotation of the camera.

            ///Here for noise for the rotation, to make it more random.
            float rotationSpeed = SimulationSettings.rotationSpeed; 
            float rotationNoiseAmount = SimulationSettings.rotationRandomAmount;

            transform.LookAt(nextPostition);
            Quaternion rotationNoise = Quaternion.Euler(rotationSpeed * Random.Range(-rotationNoiseAmount, rotationNoiseAmount), 
                                                        rotationSpeed * Random.Range(-rotationNoiseAmount, rotationNoiseAmount), 
                                                        rotationSpeed * Random.Range(-rotationNoiseAmount, rotationNoiseAmount));
            transform.rotation *= rotationNoise;
            
        }
    }

    /// <summary>
    /// Not Used Anymore
    /// </summary>
    /// <param name="positions"></param>
    public void moveTheCamera(Vector3[] positions){
        this.positions = positions;
        transform.position = positions[0];
        nextPostition = positions[1];
    }
}
