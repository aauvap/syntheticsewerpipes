# Generating Synthetic Point Clouds of SewerNetworks: Code implementation
This repository contains our implementation of our aproach to synthetically generate annotated datasets to train an AI model for classifying defects in a sewer network, as described in our paper:  
*Generating Synthetic Point Clouds of SewerNetworks: An Initial Investigation*

![Image of pipe models](readme_images/pipe_models.png)

### Dependencies

* Unity 2019.4

### Running the code:

1. Open the Unity Project in unity and select SewerGenerationScene
1. Set display size to 224 x 171 px
1. Run the project and select the game tab
1. Select where to save the output with the *path* field
1. Select how many good point-clouds are generated via the *fine* field, and how many pointclouds containing defects with the *defect* field. 
1. Hit generate


### License
All code is licensed under the MIT license. License restrictions from Unity still applies.

### Acknowledgements
Please cite the following paper if you use our code or dataset:

```TeX
@InProceedings{Henriksen_2020_AVR,
author = {Henriksen, Kasper Sch{\o}n and Lynge, Mathias S. and Jeppesen, Mikkel D. B. and Allahham, Moaaz M. J. and Nikolov, Ivan A. and Haurum, Joakim Bruslund and Moeslund, Thomas B.},
booktitle = {Augmented Reality, Virtual Reality, and Computer Graphics},
title = {Generating Synthetic Point Clouds of Sewer Networks: An Initial Investigation},
pages={364--373},
year = {2020},
isbn={978-3-030-58468-9}
} 
```